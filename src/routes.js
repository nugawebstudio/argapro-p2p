import React from "react";
import DefaultLayout from "./containers/DefaultLayout";

const Dashboard = React.lazy(() => import("./views/Dashboard"));
const SoIndex = React.lazy(() => import("./views/SO/SoIndex"));
const SoDetails = React.lazy(() => import("./views/SO/SoDetails"));
const SoHistory = React.lazy(() => import("./views/SO/SoHistory"));
const SoTopup = React.lazy(() => import("./views/SO/SoTopup"));
const SoWithdraw = React.lazy(() => import("./views/SO/SoWithdraw"));

const Profiles = React.lazy(() => import("./views/Profiles/Profiles"));

const SalesOrderDetail = React.lazy(() =>
  import("./views/SalesOrder/SalesOrderDetail")
);


// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: "/", exact: true, name: "Home", component: DefaultLayout },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  { path: "/so_index", name: "SO List", component: SoIndex},
  { path: "/so_details", name : "SO Details", component: SoDetails},
  { path: "/so_history", name: "SO History", component: SoHistory},
  { path: "/so_topup", name: "SO Topup", component: SoTopup},
  { path: "/so_withdraw", name: "SO Withdraw", component: SoWithdraw},


  {
    path: "/sales-order/:status/detail/:salesOrderId",
    name: "Sales Order Detail",
    component: SalesOrderDetail
  },
  { path: "/profile", name: "Edit Profile", component: Profiles},
//   {
//     path: "/demobooker",
//     exact: true,
//     name: "Demo Booker",
//     component: DemoBooker
//   },
//   {
//     path: "/demobooker-location",
//     exact: true,
//     name: "Demo Booker Locations",
//     component: DemoBookerLocations
//   },
//   {
//     path: "/demobooker/add",
//     name: "Add Demo Booker",
//     component: AddDemoBooker
//   },
//   {
//     path: "/demobooker-location/detail/:locationId",
//     name: "Demo Booker Location Detail",
//     component: DemoBookerLocationDetail
//   },
//   { path: "/sales", exact: true, name: "Sales", component: Sales },
//   { path: "/sales/add", name: "Add Sales", component: SalesDetail },
//   { path: "/sales/edit/:salesId", name: "Edit Sales", component: SalesDetail },
//   {
//     path: "/coordinator",
//     exact: true,
//     name: "Coordinator",
//     component: Coordinator
//   },
//   {
//     path: "/coordinator/add",
//     name: "Add Coordinator",
//     component: AddCoordinator
//   }
];

export default routes;
