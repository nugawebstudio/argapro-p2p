import axios from "../helper/Axios";
import { adminUrl, apiUrl } from "../config/config";
import commonStore from "../stores/CommonStore";

export async function getAllOrder(status) {
  try {
    const response = await axios.Get({
    //   url: `${adminUrl}/order?status=${status}`
    url: `${adminUrl}/order?status=draft`
    });

    if (response.statusCode !== 200) {
      return { error: response.message };
    }

    return { data: response.data };
  } catch (err) {
    commonStore.showErrorMessage(err.message);
  }
}

export async function getSalesOrderById(salesOrderId) {
  try {
    const response = await axios.Get({
      url: `${adminUrl}/order/${salesOrderId}`
    });

    if (response.statusCode !== 200) {
      return { error: response.message };
    }

    return { data: response.data };
  } catch (err) {
    commonStore.showErrorMessage(err.response.data.message);
  }
}

export async function rejectOrder(orderId, description) {
  try {
    const response = await axios.Put({
      url: `${adminUrl}/order/reject/${orderId}`,
      data: { description }
    });

    if (response.statusCode !== 200) {
      return { error: response.message };
    }

    return { data: response.data };
  } catch (err) {
    commonStore.showErrorMessage(err.message);
  }
}
