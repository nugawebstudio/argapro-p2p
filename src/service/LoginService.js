import axios from "../helper/Axios.js";
import { apiUrl, adminUrl } from "../config/config.js";

export async function loginAdmin(email, password) {
  try {
    const response = await axios.Post({
      url: `${adminUrl}/auth/login`,
      data: {
        email,
        password
      }
    });
    
    if (response.statusCode !== 200) {
      return { error: response.message };
    }

    return { data: response.data };
  } catch (e) {
    return { error: e.message };
  }
}
