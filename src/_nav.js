export default {
    items: [
      {
        name: "Dashboard",
        url: "/dashboard",
        icon: "icon-speedometer"
      },
      {
        name: "Transaction",
        icon: "cui-credit-card",
        children:[
          {
            name: "SO List",
            icon: "cui-list",
            url: "/so_index"
          },
          {
            name: "Transaction List",
            icon: "cui-cart",
            url: "/so_topup"
          },
          {
            name: "Payment Status",
            icon: "cui-credit-card",
            url: "/so_withdraw"
          },
          // {
          //   name: "History ",
          //   icon: "cui-align-center",
          //   url: "/so_history"
          // },
        ]
      },
      {
        name: "User Management",
        icon: "cui-user",
        children: [
          {
            name: "Profile",
            icon: "cui-user",
            url: "/profile"
          },
          
          
        ]
      }
    ]
  };
  