const numeral = require("numeral");

export function formatCurrency(currency) {
  if (!currency) {
    return "";
  }
  return numeral(currency).format("0,0");
}
