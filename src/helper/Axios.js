import axios from "axios";
import { getCookie } from "./cookie";

const queryParser = params => {
  let queryParams = "";
  for (let key in params) {
    if (!queryParams) {
      queryParams = queryParams.concat(`?${key}=${params[key]}`);
    } else {
      queryParams = queryParams.concat(`&${key}=${params[key]}`);
    }
  }
  return queryParams;
};

const getToken = () => {
  let token = "";

  if (getCookie("admin_token")) {
    token = getCookie("admin_token");
  }
  return token;
};

const defaultHeaders = {
  "Content-Type": "application/json"
  // 'Accept': 'application/json'
};

const Axios = {
  Get: ({ url, params }) => {
    return new Promise((resolve, reject) => {
      axios.request({
        url: url + queryParser(params),
        method: "GET",
        headers: {
          ...defaultHeaders,
          "token": getToken()
        }
      })
        .then(response => {
          resolve(response.data);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  Post: ({ url, params, data }) => {
    return new Promise((resolve, reject) => {
      axios.request({
        url: url,
        method: "POST",
        headers: {
          ...defaultHeaders,
          "token": getToken()
        },
        data: data
      })
        .then(response => {
          resolve(response.data);
        })
        .catch(error => {
          reject(error);
        });
    });
  },

  Put: ({ url, params, data }) => {
    return new Promise((resolve, reject) => {
      axios.request({
        url: url + queryParser(params),
        method: "PUT",
        headers: {
          ...defaultHeaders,
          "token": getToken()
        },
        data
      })
        .then(response => {
          resolve(response.data);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  Delete: ({ url, params, data }) => {
    return new Promise((resolve, reject) => {
      axios.request({
        url: url + queryParser(params),
        method: "DELETE",
        headers: {
          ...defaultHeaders,
          "token": getToken()
        }
      })
        .then(response => {
          resolve(response);
        })
        .catch(error => {
          reject(error);
        });
    });
  }
};

export default Axios;
