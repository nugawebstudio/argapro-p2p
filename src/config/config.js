const apiUrl = "https://argapro-1547682002896.appspot.com/api/v1";
const adminUrl = "https://argapro-1547682002896.appspot.com/admin/v1";

module.exports = { apiUrl, adminUrl };
