import React, { Component } from "react";
import { Route, Router, Switch, withRouter } from "react-router-dom";
import { Provider } from "mobx-react";
// import { renderRoutes } from 'react-router-config';
import { createBrowserHistory } from "history";
import { Login, Page404, Page500 } from "./views/Pages";
import AppSnackbar from "./controls/AppSnackbar";
import { DefaultLayout } from "./containers";
import stores from "./stores";
import "./App.scss";

class App extends Component {
  render() {
    const Root = () => (
      <Switch>
        <Route exact path="/login" name="Login Page" component={Login} />
        <Route exact path="/404" name="Page 404" component={Page404} />
        <Route exact path="/500" name="Page 500" component={Page500} />
        <Route path="/" name="Home" component={DefaultLayout} />
      </Switch>
    );

    const browserHistory = createBrowserHistory();
    const RouterArgapro = withRouter(Root);
    return (
      <Provider {...stores}>
        <React.Fragment>
          <AppSnackbar />
          <Router history={browserHistory}>
            <RouterArgapro />
          </Router>
        </React.Fragment>
      </Provider>
    );
  }
}

export default App;
