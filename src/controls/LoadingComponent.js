import React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import { withStyles } from "@material-ui/core/styles";

const styles = theme => ({
  container: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    height: "fit-content",
    margin: "20px"
  },
  loadingText: {
    fontSize: "14px",
    lineHeight: "16px"
  }
});

const LoadingComponent = ({ classes, error }) => {
  return (
    <div className={classes.container}>
      <CircularProgress color="primary" style={{ margin: "10px" }} />
      <span className={classes.loadingText}>Loading</span>
    </div>
  );
};

export default withStyles(styles)(LoadingComponent);
