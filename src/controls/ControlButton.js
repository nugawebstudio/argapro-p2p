import React from "react";

class ControlButton extends React.Component {
  static defaultProps = {
    onClick: e => {
      e.preventDefault();
    },
    buttonText: "",
    backgroundColor: "#9AC33D",
    textColor: "#ffffff",
    maxWidth: "350px",
    buttonWidth: "100%",
    buttonPadding: "10px 0",
    borderRadius: "4px",
    cursor: "pointer",
    margin: "",
    border: "none"
  };

  render() {
    const {
      buttonText,
      onClick,
      backgroundColor,
      textColor,
      maxWidth,
      buttonWidth,
      buttonPadding,
      borderRadius,
      cursor,
      margin,
      border
    } = this.props;
    return (
      <button
        className="control-button"
        style={{
          cursor: cursor,
          backgroundColor: backgroundColor,
          color: textColor,
          maxWidth: maxWidth,
          width: buttonWidth,
          padding: buttonPadding,
          border: "none",
          borderRadius: borderRadius,
          textTransform: "uppercase",
          margin: margin,
          border: border
        }}
        onClick={onClick}
      >
        {buttonText}
      </button>
    );
  }
}
export default ControlButton;
