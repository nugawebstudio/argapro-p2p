import React, { Component } from "react";
import { Row, Col } from "reactstrap";
import ControlButton from "../../controls/ControlButton";
import TextField from "@material-ui/core/TextField";
import { inject, observer } from "mobx-react";
import { withRouter } from "react-router";
import LoadingComponent from "../../controls/LoadingComponent";
import { toJS } from "mobx";
import moment from "moment";
import { SortingState, IntegratedSorting } from "@devexpress/dx-react-grid";
import {
  Grid,
  TableHeaderRow,
  TableColumnResizing,
  Table,
  VirtualTable
} from "@devexpress/dx-react-grid-material-ui";
import { formatCurrency } from "../../helper/commonFunction";

class SalesOrderDetail extends Component {
  componentDidMount() {
    const { match } = this.props;
    const { params } = match;
    const { salesOrderId } = params;

    if (salesOrderId) {
      const { salesOrderStore } = this.props;
      salesOrderStore.getSalesOrderById(salesOrderId);
    }
  }

  handleCancel = () => {
    const { salesOrderStore } = this.props;

    salesOrderStore.openRejectSalesOrderDialog();
  };

  handleSurvey = () => {
    const { salesOrderStore } = this.props;

    salesOrderStore.openAssignSurveyDialog();
  };

  Cell = props => {
    const { row, column } = props;
    const { valueCustomator } = column;
    if (valueCustomator) {
      return (
        <Table.Cell>
          <div>{row ? valueCustomator(row) : ""}</div>
        </Table.Cell>
      );
    }
    return <Table.Cell {...props} />;
  };

  renderOrderItemGrid() {
    const valueCustomatorProduct = {
      productName: data => {
        if (!data) {
          return "-";
        }
        if (!data.sku) {
          return "-";
        }
        return <span>{data.sku && data.sku.name ? data.sku.name : "-"}</span>;
      },
      productType: data => {
        if (!data) {
          return "-";
        }
        if (!data.sku) {
          return "-";
        }
        return (
          <span>
            {data.sku && data.sku.category ? data.sku.category.type : "-"}
          </span>
        );
      },
      categoryName: data => {
        if (!data) {
          return "-";
        }
        return (
          <span>
            {data.sku && data.sku.category ? data.sku.category.name : "-"}
          </span>
        );
      },
      quantity: data => {
        if (!data) {
          return "-";
        }
        return <span>1</span>;
      },
      price: data => {
        if (!data) {
          return "-";
        }
        return <span>{data.sku ? data.sku.price : "-"}</span>;
      },
      total: data => {
        if (!data) {
          return "-";
        }
        return <span>{data.sku ? data.sku.price : "-"}</span>;
      }
    };
    const defaultColumnWidth = [
      { columnName: "productName", width: 210 },
      { columnName: "productType", width: 120 },
      { columnName: "productCode", width: 180 },
      { columnName: "quantity", width: 120 },
      { columnName: "price", width: 100 },
      { columnName: "total", width: 120 }
    ];
    const columns = [
      {
        name: "productName",
        title: "Nama Produk",
        key: true,
        valueCustomator: valueCustomatorProduct["productName"]
      },
      {
        name: "productType",
        title: "Tipe Produk",
        valueCustomator: valueCustomatorProduct["productType"]
      },
      {
        name: "categoryName",
        title: "Merk",
        valueCustomator: valueCustomatorProduct["categoryName"]
      },
      {
        name: "quantity",
        title: "Jumlah Barang",
        valueCustomator: valueCustomatorProduct["quantity"]
      },
      {
        name: "price",
        title: "Harga",
        valueCustomator: valueCustomatorProduct["price"]
      },
      {
        name: "total",
        title: "Jumlah",
        valueCustomator: valueCustomatorProduct["total"]
      }
    ];
    const { salesOrderStore, match } = this.props;
    const { status } = match.params;
    const { SalesOrderArrayList, editedData } = salesOrderStore;

    return (
      <div>
        <Grid
          rows={editedData.product ? editedData.product : []}
          columns={columns}
        >
          <Row>
            <Col sm="6" md="6" lg="6" xl="6">
              <div style={{ marginBottom: "10px" }}>
                <span className="content-title">Barang Pesanan</span>
              </div>
            </Col>
            {status == "draft" && (
              <Col sm="6" md="6" lg="6" xl="6">
                {/* <ControlButton
                  buttonText={"ADD"}
                  maxWidth="160px"
                  backgroundColor="#ffffff"
                  textColor="#9AC33D"
                  float="right"
                  //   disabled={editedData.status === "inProgress" ? false : true}
                  onClick={this.handleSurvey}
                /> */}
              </Col>
            )}
          </Row>

          <SortingState
            defaultSorting={[{ columnName: "_id", direction: "asc" }]}
          />
          <IntegratedSorting />

          <VirtualTable cellComponent={this.Cell} height={250} />
          {/* <TableColumnResizing defaultColumnWidths={defaultColumnWidth} /> */}
          <TableHeaderRow showSortingControls />
        </Grid>
      </div>
    );
  }

  renderPaymentGrid() {
    const valueCustomatorUser = {
      nik: data => {
        if (!data) {
          return "-";
        }
        if (!data.sku) {
          return "-";
        }
        return <span>{data.sku && data.sku.name ? data.sku.name : "-"}</span>;
      },
      name: data => {
        if (!data) {
          return "-";
        }
        if (!data.sku) {
          return "-";
        }
        return (
          <span>
            {data.sku && data.sku.category ? data.sku.category.type : "-"}
          </span>
        );
      },
      phoneNumber: data => {
        if (!data) {
          return "-";
        }
        return (
          <span>
            {data.sku && data.sku.category ? data.sku.category.name : "-"}
          </span>
        );
      },
      address: data => {
        if (!data) {
          return "-";
        }
        return <span>1</span>;
      }
    };
    const defaultColumnWidth = [
      { columnName: "nik", width: 210 },
      { columnName: "name", width: 120 },
      { columnName: "phoneNumber", width: 180 },
      { columnName: "address", width: 120 }
    ];
    const columns = [
      { name: "nik", title: "NIK", key: true },
      { name: "name", title: "Nama" },
      { name: "phoneNumber", title: "No Telepon" },
      { name: "address", title: "Alamat" }
    ];
    const { salesOrderStore, match } = this.props;
    const { status } = match.params;
    const { SalesOrderArrayList, editedData } = salesOrderStore;

    return (
      <div>
        <Grid
          rows={editedData.endUser ? editedData.endUser : []}
          columns={columns}
        >
          <Row>
            <Col sm="6" md="6" lg="6" xl="6">
              <div style={{ marginBottom: "10px" }}>
                <span className="content-title">End Customer</span>
              </div>
            </Col>
            {status == "draft" && (
              <Col sm="6" md="6" lg="6" xl="6">
                {/* <ControlButton
                  buttonText={"ADD"}
                  maxWidth="160px"
                  backgroundColor="#ffffff"
                  textColor="#9AC33D"
                  float="right"
                  //   disabled={editedData.status === "inProgress" ? false : true}
                  onClick={this.handleSurvey}
                /> */}
              </Col>
            )}
          </Row>
          <SortingState
            defaultSorting={[{ columnName: "_id", direction: "asc" }]}
          />
          <IntegratedSorting />

          <VirtualTable cellComponent={this.Cell} height={250} />
          <TableColumnResizing defaultColumnWidths={defaultColumnWidth} />
          <TableHeaderRow showSortingControls />
        </Grid>
      </div>
    );
  }

  render() {
    const { salesOrderStore } = this.props;
    const { editedData } = salesOrderStore;

    // if (getDemobookerLocationByIdLoading) {
    //   return (
    //     <div className="animated fadeIn content-container">
    //       <Row>
    //         <Col xl="12" style={{ marginBottom: "16px" }}>
    //           <span className="content-title">Sales Order</span>
    //         </Col>
    //         <Col xl="12" style={{ marginBottom: "16px", textAlign: "center" }}>
    //           <LoadingComponent />
    //         </Col>
    //       </Row>
    //     </div>
    //   );
    // }

    return (
      <div className="animated fadeIn content-container">
        <Row>
          <Col xl="12" style={{ marginBottom: "16px" }}>
            <span className="content-title">Sales Order - # {editedData._id}</span>
          </Col>
          <Col xl="12" style={{ marginBottom: "16px" }}>
            
            <ControlButton
              buttonText={"approve"}
              maxWidth="160px"
              backgroundColor="#204179"
              textColor="#ffffff"
              onClick={this.handleApprove}
            />
          </Col>
          <Col sm="6" md="6" lg="6" xl="6" style={{ marginBottom: "10px" }}>
            <TextField
              value={editedData._id ? editedData._id : ""}
              onChange={this.handleChange}
              disabled={true}
              name="_id"
              type="text"
              label="No SO"
              placeholder="No SO"
              margin="normal"
              variant="outlined"
              className="content-input"
              style={{ maxWidth: "100%" }}
            />
          </Col>
          <Col sm="6" md="6" lg="6" xl="6" style={{ marginBottom: "10px" }}>
            <TextField
              value={editedData.status ? editedData.status : ""}
              onChange={this.handleChange}
              disabled={true}
              name="status"
              type="text"
              label="Status"
              placeholder="Status"
              margin="normal"
              variant="outlined"
              className="content-input"
              style={{ maxWidth: "100%" }}
            />
          </Col>
          <Col sm="6" md="6" lg="6" xl="6" style={{ marginBottom: "10px" }}>
            <TextField
              value={editedData.createdDate ? editedData.createdDate : ""}
              onChange={this.handleChange}
              disabled={true}
              name="createdDate"
              type="text"
              label="Tanggal SO"
              placeholder="Tanggal SO"
              margin="normal"
              variant="outlined"
              className="content-input"
              style={{ maxWidth: "100%" }}
            />
          </Col>
          <Col sm="6" md="6" lg="6" xl="6" style={{ marginBottom: "10px" }}>
            {/* <TextField
              //   value={editedData.latitude ? editedData.latitude : ""}
              onChange={this.handleChange}
              disabled={true}
              name="surveyDate"
              type="text"
              label="Tanggal Survey"
              placeholder="Tanggal Survey"
              margin="normal"
              variant="outlined"
              className="content-input"
              style={{ maxWidth: "100%" }} 
            />*/}
          </Col>
          {/* <Col xl="12" style={{ marginBottom: "20px" }}>
            <TextField
              //   value={editedData.description ? editedData.description : ""}
              onChange={this.handleChange}
              disabled={true}
              name="deliveryDate"
              type="text"
              label="Tanggal Pengiriman"
              placeholder="Tanggal Pengiriman"
              margin="normal"
              variant="outlined"
              className="content-input"
              style={{ maxWidth: "100%" }}
            />
          </Col> */}
        </Row>

        <Row>
          <Col sm="6" md="6" lg="6" xl="6" style={{ marginBottom: "10px" }}>
            <TextField
              value={
                editedData.demoLocation && editedData.demoLocation.demoBooker
                  ? editedData.demoLocation.demoBooker.name
                  : ""
              }
              onChange={this.handleChange}
              disabled={true}
              name="demobooker"
              type="text"
              label="Demo Booker"
              placeholder="Demo Booker"
              margin="normal"
              variant="outlined"
              className="content-input"
              style={{ maxWidth: "100%" }}
            />
          </Col>
          <Col sm="6" md="6" lg="6" xl="6" style={{ marginBottom: "10px" }}>
            <TextField
              value={editedData.sales ? editedData.sales.name : ""}
              onChange={this.handleChange}
              disabled={true}
              name="salesPromotor"
              type="text"
              label="Sales Promotor"
              placeholder="Sales Promotor"
              margin="normal"
              variant="outlined"
              className="content-input"
              style={{ maxWidth: "100%" }}
            />
          </Col>
          {/* <Col sm="6" md="6" lg="6" xl="6" style={{ marginBottom: "10px" }}>
            <TextField
              //   value={editedData.longitude ? editedData.longitude : ""}
              onChange={this.handleChange}
              disabled={true}
              name="spvSales"
              type="text"
              label="SPV Sales"
              placeholder="SPV Sales"
              margin="normal"
              variant="outlined"
              className="content-input"
              style={{ maxWidth: "100%" }}
            />
          </Col>
          <Col sm="6" md="6" lg="6" xl="6" style={{ marginBottom: "10px" }}>
            <TextField
              //   value={editedData.longitude ? editedData.longitude : ""}
              onChange={this.handleChange}
              disabled={true}
              name="spvCredit"
              type="text"
              label="SPV Credit"
              placeholder="SPV Credit"
              margin="normal"
              variant="outlined"
              className="content-input"
              style={{ maxWidth: "100%" }}
            />
          </Col> */}
          {/* <Col sm="6" md="6" lg="6" xl="6" style={{ marginBottom: "20px" }}>
            <TextField
              value={editedData.surveyor ? editedData.surveyor : ""}
              onChange={this.handleChange}
              disabled={true}
              name="surveyor"
              type="text"
              label="Surveyor"
              placeholder="Surveyor"
              margin="normal"
              variant="outlined"
              className="content-input"
              style={{ maxWidth: "100%" }}
            />
          </Col> */}
        </Row>

        <Row>
          <Col sm="6" md="6" lg="6" xl="6" style={{ marginBottom: "10px" }}>
            <TextField
              value={editedData.coordinator ? editedData.coordinator.name : ""}
              onChange={this.handleChange}
              disabled={true}
              name="coordinator"
              type="text"
              label="Coordinator"
              placeholder="Coordinator"
              margin="normal"
              variant="outlined"
              className="content-input"
              style={{ maxWidth: "100%" }}
            />
          </Col>
          <Col sm="6" md="6" lg="6" xl="6" style={{ marginBottom: "10px" }}>
            <TextField
              value={editedData.coordinator ? editedData.coordinator.nik : ""}
              onChange={this.handleChange}
              disabled={true}
              name="status"
              type="text"
              label="NIK"
              placeholder="NIK"
              margin="normal"
              variant="outlined"
              className="content-input"
              style={{ maxWidth: "100%" }}
            />
          </Col>
          <Col xl="12" style={{ marginBottom: "10px" }}>
            <TextField
              value={editedData.coord ? editedData.coordinator.address : ""}
              onChange={this.handleChange}
              disabled={true}
              name="longitude"
              type="text"
              label="Address"
              placeholder="Address"
              margin="normal"
              variant="outlined"
              className="content-input"
              style={{ maxWidth: "100%" }}
            />
          </Col>
          <Col sm="6" md="6" lg="6" xl="6" style={{ marginBottom: "25px" }}>
            <TextField
              value={
                editedData.coordinator ? editedData.coordinator.phoneNumber : ""
              }
              onChange={this.handleChange}
              disabled={true}
              name="latitude"
              type="text"
              label="Phone Number"
              placeholder="Phone Number"
              margin="normal"
              variant="outlined"
              className="content-input"
              style={{ maxWidth: "100%" }}
            />
          </Col>
          <Col xl="12" style={{ marginBottom: "10px" }}>
            <div style={{ display: "flex", flexDirection: "column" }}>
              {this.renderOrderItemGrid()}
              <div
                style={{
                  width: "180px",
                  alignSelf: "flex-end",
                  marginTop: "15px"
                }}
              >
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between"
                  }}
                >
                  <span className="content-article">Sub Total</span>
                  <span
                    className="content-article"
                    style={{ fontWeight: "bold" }}
                  >
                    Rp.{" "}
                    {editedData && editedData.grandTotal
                      ? editedData.grandTotal
                      : 0}
                  </span>
                </div>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between"
                  }}
                >
                  <span className="content-article">Discount</span>
                  <span
                    className="content-article"
                    style={{ fontWeight: "bold" }}
                  >
                    Rp. 0
                  </span>
                </div>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between"
                  }}
                >
                  <span className="content-article">Amount</span>
                  <span
                    className="content-article"
                    style={{ fontWeight: "bold" }}
                  >
                    Rp.{" "}
                    {editedData && editedData.grandTotal
                      ? editedData.grandTotal
                      : 0}
                  </span>
                </div>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between"
                  }}
                >
                  <span className="content-article">1st Payment</span>
                  <span
                    className="content-article"
                    style={{ fontWeight: "bold" }}
                  >
                    Rp. 0
                  </span>
                </div>
              </div>
            </div>
          </Col>
          <Col xl="12" style={{ marginBottom: "10px" }}>
            <div styel={{ display: "flex", flexDirection: "column" }}>
              {this.renderPaymentGrid()}
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default withRouter(
  inject("salesOrderStore")(observer(SalesOrderDetail))
);
