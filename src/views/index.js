import Dashboard from "./Dashboard";
import { Login, Page404, Page500, Register } from "./Pages";
// import DemoBooker from "./DemoBooker";
// import DemoBookerLocations from "./DemoBooker/DemoBookerLocations";
// import AddDemoBooker from "./DemoBooker/AddDemoBooker";
// import DemoBookerLocationDetail from "./DemoBooker/DemoBookerLocationDetail";
// import Sales from "./Sales";
// import SalesDetail from " ./Sales/SalesDetail";
// import Coordinator from "./Coordinator";
// import AddCoordinator from "./Coordinator/AddCoordinator";

export {
  Page404,
  Page500,
  Register,
  Login,
  Dashboard,
//   DemoBooker,
//   DemoBookerLocations,
//   AddDemoBooker,
//   Sales,
//   SalesDetail,
//   Coordinator,
//   AddCoordinator,
//   DemoBookerLocationDetail
};
