import React, { Component } from 'react'

import {  Row, Col } from "reactstrap";
import ControlButton from "../../controls/ControlButton";
import TextField from "@material-ui/core/TextField";
class Profiles extends Component {
  render() {
    return (
      <div className="white-box">
        <h4>Profile</h4>
        <div className="animated fadeIn content-container">
        <Row>
        <Col sm="6" md="6" lg="6" xl="6" style={{ marginBottom: "10px" }}>
          <TextField
              value="Ariel Azzam"
              name="nama"
              type="text"
              label="Nama"
              placeholder="Nama"
              margin="normal"
              variant="outlined"
              className="content-input"
              style={{ maxWidth: "100%" }}
            />
        </Col>
        <Col sm="6" md="6" lg="6" xl="6" style={{ marginBottom: "10px" }}>
            <TextField
              value="3209353460034634623"
              name="nik"
              type="text"
              label="NIK"
              placeholder="NIK"
              margin="normal"
              variant="outlined"
              className="content-input"
              style={{ maxWidth: "100%" }}
            />
        </Col>
        <Col sm="6" md="6" lg="6" xl="6" style={{ marginBottom: "10px" }}>
            <TextField
              value="a@tes.com"
              name="email"
              type="text"
              label="Email"
              placeholder="Email"
              margin="normal"
              variant="outlined"
              className="content-input"
              style={{ maxWidth: "100%" }}
            />
        </Col>
        <Col sm="6" md="6" lg="6" xl="6" style={{ marginBottom: "10px" }}>
            <TextField
              value="Lender"
              name="lender"
              type="text"
              label="Posisi"
              placeholder="Posisi"
              margin="normal"
              variant="outlined"
              className="content-input"
              style={{ maxWidth: "100%" }}
            />
        </Col>
        <Col sm="6" md="6" lg="6" xl="6" style={{ marginBottom: "10px" }}>
            <TextField
              value=""
              name="password"
              type="password"
              label="Password"
            
              placeholder="*************"
              margin="normal"
              variant="outlined"
              className="content-input"
              style={{ maxWidth: "100%" }}
            />
        </Col>
        </Row>
        <Row>
        <Col xl="12" style={{ marginBottom: "16px" }}>
            
            <ControlButton
              buttonText={"SIMPAN"}
              maxWidth="160px"
              backgroundColor="#204179"
              textColor="#ffffff"
              onClick={this.handleApprove}
            />
          </Col>
        </Row>


        </div>
      </div>
    )
  }
}


export default Profiles;