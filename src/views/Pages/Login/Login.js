import React, { Component } from "react";
import { Col, Row } from "reactstrap";
import TextField from "@material-ui/core/TextField";
import ControlButton from "../../../controls/ControlButton";
import { inject, observer } from "mobx-react";

const logo = require("../../../assets/icons/argapro2.png");
class Login extends Component {

  handleChange = (event) => {
    const { name, value } = event.target;
    this.props.authStore.setEditedData({
      [name]: value
    });
  }

  handleLoginClick = () => {
    const { authStore, history } = this.props;
    const { editedData } = authStore;
    authStore.adminLogin(editedData.email, editedData.password, history);
  };

  render() {
    const { editedData } = this.props.authStore;
    return (
      <div className="login-container">
        <Row style={{ height: "100%" }}>
          <Col xl="12" className="login-container-form">
            <img src={logo} alt="logo" />
            <span className="content-title">login</span>
            <TextField
              name="email"
              value={editedData.email ? editedData.email : ""}
              onChange={this.handleChange}
              label="Email"
              placeholder="Email"
              margin="normal"
              variant="outlined"
              className="content-input"
            />
            <TextField
              name="password"
              value={editedData.password ? editedData.password : ""}
              onChange={this.handleChange}
              type="password"
              label="Password"
              placeholder="Password"
              margin="normal"
              variant="outlined"
              className="content-input"
            />
            <ControlButton
              onClick={this.handleLoginClick}
              buttonText="login"
              maxWidth="350px"
              padding="16px 0"
              hoverBackgroundColor="#204179"
              hoverTextColor="#f2f5f9"
              margin="10px 0 0 0"
            />
          </Col>
        </Row>
      </div>
    );
  }
}

export default inject("authStore")(observer(Login));
