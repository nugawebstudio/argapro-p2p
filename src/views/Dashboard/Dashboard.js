import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import { inject, observer } from "mobx-react";
// import LoadingComponent from "../../controls/LoadingComponent";
import {
  PagingState,
  IntegratedPaging,
  SortingState,
  IntegratedSorting,
  SearchState,
} from "@devexpress/dx-react-grid";
import {
  Grid,
  PagingPanel,
  TableHeaderRow,
  Table,
  SearchPanel,Toolbar
} from "@devexpress/dx-react-grid-material-ui";
import { formatCurrency } from "../../helper/commonFunction";
import { toJS } from "mobx";


class Dashboard extends Component {

    componentDidMount(){
        const { match , salesOrderStore } = this.props;
        const { params } = match;
        const { status } = params;
        salesOrderStore.getAllSalesOrder(status);
    }

    componentWillReceiveProps(nextProps){
        const { match, salesOrderStore } = this.props;
        const { params } = match;
        const { status } = params;

        if (nextProps.status !== status) {
        salesOrderStore.getAllSalesOrder(nextProps.match.params.status);
        }
    }
    Cell = props => {
        if (props.column.name === "total") {
          return (
            <Table.Cell>
              <span>Rp. {formatCurrency(props.value)}</span>
            </Table.Cell>
          );
        }
        if (props.column.name === "status") {
          if (props.value === "draft") {
            return (
              <Table.Cell>
                <span style={{ color: "#7ED321" }}>Baru</span>
              </Table.Cell>
            );
          } else if (props.value === "onGoing") {
            return (
              <Table.Cell>
                <span style={{ color: "#F5A623" }}>Berjalan</span>
              </Table.Cell>
            );
          } else if (props.value === "approved") {
            return (
              <Table.Cell>
                <span style={{ color: "#9B9B9B" }}>Selesai</span>
              </Table.Cell>
            );
          } else if (props.value === "rejected") {
            return (
              <Table.Cell>
                <span style={{ color: "#D0021B" }}>Ditolak</span>
              </Table.Cell>
            );
          }
        }
        return <Table.Cell {...props} />;
      };

      TableRow = ({ row, ...restProps }) => (
        <Table.Row
          {...restProps}
          onClick={e => {
            e.preventDefault();
            const { history, match } = this.props;
            const { status } = this.props.match.params;
            const salesOrderId = row._id;
            history.push(`/sales-order/${status}/detail/${salesOrderId}`);
          }}
          className="row-hover"
        />
      );
    
    renderDashboard() {
        const columns = [
            { name: "noSo", title: "Nomor SO", key: true },
            { name: "date", title: "Tanggal & Waktu" },
            { name: "coordinator", title: "Coordinator" },
            { name: "total", title: "Total" },
            { name: "status", title: "Status" }
          ];
          const pageSize = 10;
          const { salesOrderStore } = this.props;
          const { SalesOrderArrayList } = salesOrderStore;
      
        return (
            <div>
            <Grid rows={SalesOrderArrayList} columns={columns}>
              <SortingState
                defaultSorting={[{ columnName: "_id", direction: "asc" }]}
              />
              <SearchState />
              <IntegratedSorting />
    
              <PagingState defaultCurrentPage={0} pageSize={pageSize} />
              <IntegratedPaging />
              <PagingPanel />
    
              <Table cellComponent={this.Cell} rowComponent={this.TableRow} />
              <TableHeaderRow showSortingControls />
              <Toolbar />
              <SearchPanel />
            </Grid>
          </div>
        );
      }
  render() {
    const { salesOrderStore } = this.props;
    const { getAllSalesOrderLoading } = salesOrderStore;

    return <Container >
            <Row className="justify-content-center" >
                <Col xl="3" >
                <div className="card-balance">
                    <h3 >Rp. 200.000.000</h3>
                    <h5>TOTAL TRANSACTION</h5>
                </div>
                </Col>
                <Col xl="3">
                <div className="card-balance">
                    <h3>Rp. 180.000.000</h3>
                    <h5>TOTAL WITHDRAW</h5>
                </div>
                </Col>
                <Col xl="3">
                <div className="card-balance">
                    <h3>Rp. 20.000.000</h3>
                    <h5>BALANCE</h5>
                </div>
                </Col>
                <Col xl="3">
                <div className="card-balance">
                    <h3>Rp. 0</h3>
                    <h5>WITHDRAW PROCCESS</h5>
                </div>
                </Col>

            </Row>

            <Row className="">
            <div className="col-md-12">
            <hr></hr>
            <div className="animated fadeIn">
                <div className="row">
                    <div className="col-sm-6 col-lg-3">
                        <div className="card text-white bg-success">
                        <div className="card-body pb-0">
                        <div className="btn-group float-right">
                               <i className="cui-file" styles={{fontSize: "24px",}}></i>
                            
                            </div>
                            <div className="text-value">9.823</div>
                            <div>NEW SO</div>
                        </div>
                        
                        </div>
                    </div>
                    <div className="col-sm-6 col-lg-3">
                        <div className="card text-white bg-warning">
                        <div className="card-body pb-0">
                            <div className="text-value">9.823</div>
                            <div>SO WILL EXPIRE</div>
                        </div>
                        
                        </div>
                    </div>
                    <div className="col-sm-6 col-lg-3">
                        <div className="card text-white bg-danger">
                        <div className="card-body pb-0">
                            <div className="text-value">9.823</div>
                            <div>SO EXPIRED</div>
                        </div>
                        
                        </div>
                    </div>
                    <div className="col-sm-6 col-lg-3">
                        <div className="card text-white bg-primary">
                        <div className="card-body pb-0">
                            <div className="text-value">9.823</div>
                            <div>SO APPROVED</div>
                        </div>
                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
            </Row>
            
            <div className="animated fadeIn content-container" >
                <Col xl="12">{this.renderDashboard()}</Col>
            </div>

            </Container>
            
  }
}

export default inject("salesOrderStore")(observer(Dashboard));
