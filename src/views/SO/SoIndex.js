import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import { inject, observer } from "mobx-react";
import LoadingComponent from "../../controls/LoadingComponent";
import {
  PagingState,
  IntegratedPaging,
  SortingState,
  IntegratedSorting,
  SearchState
} from "@devexpress/dx-react-grid";
import {
  Grid,
  PagingPanel,
  TableHeaderRow,
  TableColumnResizing,
  Table,
  Toolbar,
  SearchPanel
} from "@devexpress/dx-react-grid-material-ui";
import { formatCurrency } from "../../helper/commonFunction";
import { toJS } from "mobx";

class SoIndex extends Component {
    componentDidMount(){
      const { match , salesOrderStore } = this.props;
        const { params } = match;
        const { status } = params;
        salesOrderStore.getAllSalesOrder(status);
    }

    componentWillReceiveProps(nextProps){
        const { match, salesOrderStore } = this.props;
        const { params } = match;
        const { status } = params;

        if (nextProps.status !== status) {
        salesOrderStore.getAllSalesOrder(nextProps.match.params.status);
        }
    }
    Cell = props => {
      if (props.column.name === "total") {
        return (
          <Table.Cell>
            <span>Rp. {formatCurrency(props.value)}</span>
          </Table.Cell>
        );
      }
      if (props.column.name === "status") {
        if (props.value === "draft") {
          return (
            <Table.Cell>
              <span style={{ color: "#7ED321" }}>Baru</span>
            </Table.Cell>
          );
        } else if (props.value === "onGoing") {
          return (
            <Table.Cell>
              <span style={{ color: "#F5A623" }}>Berjalan</span>
            </Table.Cell>
          );
        } else if (props.value === "approved") {
          return (
            <Table.Cell>
              <span style={{ color: "#9B9B9B" }}>Selesai</span>
            </Table.Cell>
          );
        } else if (props.value === "rejected") {
          return (
            <Table.Cell>
              <span style={{ color: "#D0021B" }}>Ditolak</span>
            </Table.Cell>
          );
        }
      }
      return <Table.Cell {...props} />;
    };

    TableRow = ({ row, ...restProps }) => (
      <Table.Row
        {...restProps}
        onClick={e => {
          e.preventDefault();
          const { history, match } = this.props;
          const { status } = this.props.match.params;
          const salesOrderId = row._id;
          history.push(`/sales-order/${status}/detail/${salesOrderId}`);
        }}
        className="row-hover"
      />
    );

    renderGrid() {
      const columns = [
        { name: "noSo", title: "Nomor SO", key: true },
        { name: "date", title: "Tanggal & Waktu" },
        { name: "coordinator", title: "Coordinator" },
        { name: "total", title: "Total" },
        { name: "status", title: "Status" }
      ];
      const pageSize = 10;
      const { salesOrderStore } = this.props;
      const { SalesOrderArrayList } = salesOrderStore;

        return (
          <div>
            <Grid rows={SalesOrderArrayList} columns={columns}>
              <SortingState
                defaultSorting={[{ columnName: "_id", direction: "asc" }]}
              />
              <SearchState />
              <IntegratedSorting />
    
              <PagingState defaultCurrentPage={0} pageSize={pageSize} />
              <IntegratedPaging />
              <PagingPanel />
    
              <Table cellComponent={this.Cell} rowComponent={this.TableRow} />
              <TableHeaderRow showSortingControls />
              <Toolbar />
              <SearchPanel />
            </Grid>
          </div>
        );
      }
      render() {
        const { salesOrderStore } = this.props;
        const { getAllSalesOrderLoading } = salesOrderStore;

        if (getAllSalesOrderLoading) {
          return (
            <div className="animated fadeIn content-container">
              <Row>
                <Col xl="12" style={{ marginBottom: "16px", textAlign: "center" }}>
                  <span className="content-title">Ongoing Sales Order</span>
                </Col>
                <Col xl="12" style={{ marginBottom: "16px", textAlign: "center" }}>
                  <LoadingComponent />
                </Col>
              </Row>
            </div>
          );
        }

        return (
          <div className="animated fadeIn content-container">
            <Row>
              <Col xl="12" style={{ marginBottom: "16px", textAlign: "center" }}>
                <span className="content-title">Sales Order List</span>
              </Col>
              <Col xl="12">{this.renderGrid()}</Col>
            </Row>
          </div>
        );
     
            
  }
}

export default inject("salesOrderStore")(observer(SoIndex));;

