import React, { Component } from 'react';
import TextField from "@material-ui/core/TextField";
import { Grid, Table, TableHeaderRow } from '@devexpress/dx-react-grid-material-ui';

import { Container, Row, Col } from "reactstrap";
class SoDetails extends Component {
  render() {
    return ( <Container>
      <div className="white-box">
          <h4>Sales Order - #SO37368883</h4>

            <a href="#" className="btn btn-primary btn-sm">APPROVE</a>

            <div className="">

                <Row>
                    <Col xl="6">
                    <TextField
                        name="so_number"
                        label="So Number"
                        placeholder="So Number"
                        margin="normal"
                        variant="outlined"
                        value="#SO37368883"
                        className="form-control"
                        />
                    <TextField
                        name="so_number"
                        label="So Date"
                        placeholder="SO Date"
                        value="2019-02-15 15:00"
                        margin="normal"
                        variant="outlined"
                        className="form-control"
                        />
                    
                    <TextField
                        name="so_number"
                        label="Coordinator"
                        placeholder="Coordinator"
                        margin="normal"
                        value="SO3736883"
                        variant="outlined"
                        className="form-control"
                        />
                    <TextField
                        name="so_number"
                        label="Address"
                        placeholder="Address"
                        value="Jl Dermaga No 25 RT 03 RW 02"
                        margin="normal"
                        multiline="true"
                        variant="outlined"
                        className="form-control input-lg"
                        />
                    
                    </Col>
                    <Col xl="6">
                    <TextField
                        name="so_number"
                        label="Status"
                        placeholder="Status"
                        margin="normal"
                        value="New"
                        variant="outlined"
                        className="form-control"
                        />
                        <TextField
                        name="so_number"
                        label="Survey Date"
                        placeholder="Survey Date"
                        value="2019-02-10"
                        margin="normal"
                        variant="outlined"
                        className="form-control"
                        />
                        <TextField
                        name="so_number"
                        label="NIK"
                        placeholder="NIK"
                        margin="normal"
                        value="3200012230304634634"
                        variant="outlined"
                        className="form-control"
                        />
                    <TextField
                        name="so_number"
                        label="Phone Number"
                        placeholder="Phone Number"
                        margin="normal"
                        value="0834573460034"
                        variant="outlined"
                        className="form-control"
                        />
                    </Col>
                </Row>

            </div>
            <hr></hr>
            <div className="">

                <h5>Order Items: </h5>

                {/* <Grid
                    rows={[
                    { id: 0, product_name: 'Kompor Rinnai Api Biru',product_type:"AA1", product_code: 'KLA62789934', qty : 1, price: "Rp 200.000", total: "Rp 200.000" },
                    { id: 0, product_name: 'Kompor Rinnai Api Biru 2',product_type:"AA2", product_code: 'KLA62789935', qty : 1, price: "Rp 200.000", total: "Rp 200.000" },
                    { id: 0, product_name: 'Kompor Rinnai Api Biru 3',product_type:"AA3", product_code: 'KLA62789936', qty : 1, price: "Rp 200.000", total: "Rp 200.000" },
                    
                    ]}

                    columns={[
                    { name: 'product_name', title: 'Product Name' },
                    { name: 'product_type', title: 'Product Type' },
                    { name: 'product_code', title: 'Product Code' },
                    { name: 'qty', title: 'Quantity' },
                    { name: 'price', title: 'Price' },
                    { name: 'total', title: 'Total' },
                    ]}
                     
                    >
                    <Table/>
                    
                    <TableHeaderRow />
                    
                </Grid> */}
                {/* <div className=""> */}
                    <table className="table table-striped">
                    <tr>
                        <th>Product Name</th>
                        <th>Product Type</th>
                        <th>Product Code</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Total</th>
                    </tr>
                    <tbody>
                        <tr>
                            <td className="text-center">
                              <img src="https://assets.bmdstatic.com/assets/Data/Inventory/Overview/image/Pictds%20dure1.jpg" width="100px" height="50px" />  Kompor Rinnai Api Biru
                            </td>
                            <td>AA1</td>
                            <td>KLA62789934</td>
                            <td>1</td>
                            <td>Rp 200.000</td>
                            <td>Rp 200.000</td>
                        </tr>
                        <tr>
                            <td className="text-center">
                            <img src="https://assets.bmdstatic.com/assets/Data/Inventory/Overview/image/Pictds%20dure1.jpg" width="100px" height="50px" /> Kompor Rinnai Api Biru
                            </td>
                            <td>AA1</td>
                            <td>KLA62789934</td>
                            <td>1</td>
                            <td>Rp 200.000</td>
                            <td>Rp 200.000</td>
                        </tr>
                        <tr>
                            <td className="text-center">
                            <img src="https://assets.bmdstatic.com/assets/Data/Inventory/Overview/image/Pictds%20dure1.jpg" width="100px" height="50px" />  Kompor Rinnai Api Biru
                            </td>
                            <td>AA1</td>
                            <td>KLA62789934</td>
                            <td>1</td>
                            <td>Rp 200.000</td>
                            <td>Rp 200.000</td>
                        </tr>
                    </tbody>
                    <tfoot style={{ color: "#204179", fontSize: "14px", fontWeight: "bold"}}>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td >Sub Total : </td>
                            <td>Rp 850.000</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td >Discount : </td>
                            <td>Rp 50.000</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td >Amount : </td>
                            <td>Rp 800.000</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td >1st Payment : </td>
                            <td>Rp 100.000</td>
                        </tr>
                    </tfoot>
                    </table>
                {/* </div> */}
            </div>
      </div>
      </Container>
    )
  }
}


export default SoDetails;