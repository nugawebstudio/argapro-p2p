import commonStore from "./CommonStore";
// import salesStore from "./SalesStore";
import authStore from "./AuthStore";
import salesOrderStore from "./SalesOrderStore";
// import demobookerStore from "./DemobookerStore";
// import provinceStore from "./ProvinceStore";
// import regencyStore from "./RegencyStore";
// import districtStore from "./DistrictStore";
// import villageStore from "./VillageStore";

export default {
  commonStore,
  salesOrderStore,
  authStore,
//   demobookerStore,
//   provinceStore,
//   regencyStore,
//   districtStore,
//   villageStore
};
