import { action, decorate, observable, toJS } from "mobx";
import { setCookie, getCookie, deleteCookiesByName } from "../helper/cookie";
import commonStore from "./CommonStore";
import { loginAdmin } from "../service/LoginService";
import { reactLocalStorage } from "reactjs-localstorage";

class AuthStore {
  authLoading = false;
  editedData = {};
  setEditedData = (updatedData) => {
    this.editedData = {
      ...this.editedData,
      ...updatedData
    };
  };

  adminLogin = async (email, password, history) => {
    this.authLoading = true;
    try {
      const { data, error } = await loginAdmin(email, password);

      if (error) {
        this.authLoading = false;
        commonStore.showErrorMessage(error);
      }
      else {
        this.authLoading = false;
        commonStore.showSuccessMessage("Login Success");
        reactLocalStorage.setObject("currentUser", data.user);
        setCookie("admin_token", data.token, 86400000);
        setCookie("admin_id", data.user._id, 86400000);
        history.push("./dashboard");
      }
    } catch (err) {
      this.authLoading = false;
      commonStore.showErrorMessage(err.message);
    }
  };

  adminLogout = () => {
    deleteCookiesByName("admin_token");
    deleteCookiesByName("admin_id");
    reactLocalStorage.remove("currentUser");
    window.location.href = "/login";
  };
}

decorate(AuthStore, {
  editedData: observable,
  authLoading: observable,
  adminLogin: action,
  adminLogout: action
});

export default new AuthStore();
