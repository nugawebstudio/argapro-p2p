import { action, decorate, observable, toJS } from "mobx";
import {
  getAllOrder,
  rejectOrder,
  getSalesOrderById
} from "../service/SalesOrderService";
import commonStore from "./CommonStore";
import moment from "moment";

class SalesOrderStore {
  SalesOrderArrayList = [];
  editedData = {};
  getAllSalesOrderLoading = false;
  shouldOpenRejectSalesOrderDialog = false;
  shouldOpenAssignSurveyDialog = false;
  orderId = null;
  rejectOrderLoading = false;

  setEditedData = updatedData => {
    this.editedData = {
      ...this.editedData,
      ...updatedData
    };
  };

  openRejectSalesOrderDialog = () => {
    // this.orderId = id;
    this.shouldOpenRejectSalesOrderDialog = true;
  };

  closeRejectSalesOrderDialog = () => {
    this.shouldOpenRejectSalesOrderDialog = false;
  };

  openAssignSurveyDialog = () => {
    // this.orderId = id;
    this.shouldOpenAssignSurveyDialog = true;
  };

  closeAssignSurveyDialog = () => {
    this.shouldOpenAssignSurveyDialog = false;
  };

  getAllSalesOrder = async status => {
    this.getAllSalesOrderLoading = true;

    try {
      const response = await getAllOrder(status);

      if (response) {
        this.SalesOrderArrayList = response.data.map(row => {
          const { _id, updatedDate, sales, grandTotal } = row;

          const createDate = moment(updatedDate).format("DD MM YYYY h:mm");
          return {
            ...row,
            noSo: _id,
            date: createDate,
            coordinator: sales.name,
            total: grandTotal
          };
        });
        commonStore.showSuccessMessage("Fetch data success!");
        this.getAllSalesOrderLoading = false;
      }
    } catch (err) {
      commonStore.showErrorMessage(err.message);
      this.getAllSalesOrderLoading = false;
    }
  };

  getSalesOrderById = async salesOrderId => {
    this.getAllSalesOrderLoading = true;

    try {
      const response = await getSalesOrderById(salesOrderId);

      if (response) {
        this.editedData = response.data;
        commonStore.showSuccessMessage("Fetch data success!");
        this.getAllSalesOrderLoading = false;
      }
    } catch (err) {
      commonStore.showErrorMessage(err.message);
      this.getAllSalesOrderLoading = false;
    }
  };

  rejectOrder = async (orderId, description) => {
    this.rejectOrderLoading = true;

    try {
      const response = await rejectOrder(orderId, description);

      if (response) {
        commonStore.showSuccessMessage("Sales order rejected!");
        this.rejectOrderLoading = false;
      }
    } catch (err) {
      commonStore.showErrorMessage(err.message);
      this.rejectOrderLoading = false;
    }
  };
}

decorate(SalesOrderStore, {
  SalesOrderArrayList: observable,
  editedData: observable,
  getAllSalesOrderLoading: observable,
  shouldOpenRejectSalesOrderDialog: observable,
  shouldOpenAssignSurveyDialog: observable,
  orderId: observable,
  rejectOrderLoading: observable,

  getAllSalesOrder: action,
  openRejectSalesOrderDialog: action,
  closeRejectSalesOrderDialog: action,
  openAssignSurveyDialog: action,
  closeAssignSurveyDialog: action,
  setEditedData: action,
  rejectOrder: action
});

export default new SalesOrderStore();
