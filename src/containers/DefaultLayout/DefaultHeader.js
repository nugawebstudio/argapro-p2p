import React, { Component } from "react";
import {
  Badge,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav,
  NavItem,
  NavLink
} from "reactstrap";
import PropTypes from "prop-types";

import {
  AppHeaderDropdown,
  AppNavbarBrand,
  AppSidebarToggler
} from "@coreui/react";
import { inject, observer } from "mobx-react";

const propTypes = {
  children: PropTypes.node
};

const defaultProps = {};

const logo = require("../../assets/icons/argapro2.png");
class DefaultHeader extends Component {
  handleLogoutClick = () => {
    const { authStore } = this.props;
    authStore.adminLogout();
  };

  render() {
    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: logo, width: 89, height: 25, alt: "ArgaPro Logo" }}
          minimized={{
            src: logo,
            width: 30,
            height: 30,
            alt: "ArgaPro Logo"
          }}
        />
        {/* <AppSidebarToggler className="d-md-down-none" display="lg" /> */}

        <Nav className="ml-auto" navbar>
          <AppHeaderDropdown direction="down" >
            <DropdownToggle nav>
            <a className="nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="true">
              <i className="icon-envelope-letter"></i>
              <span className="badge badge-pill badge-info">7</span>
              </a>

            </DropdownToggle>
            <DropdownMenu >
              <DropdownItem >
              Notifications
              </DropdownItem>
              <DropdownItem>
              <div className="message">
                <div className="py-3 mr-3 float-left">
                  <div className="avatar">
                    <img className="img-avatar" src="" alt="" />
                    <span className="avatar-status badge-success"></span>
                  </div>
                </div>
                <div>
                  <small className="text-muted">John Doe</small>
                  <small className="text-muted float-right mt-1">Just now</small>
                </div>
                <div className="text-truncate font-weight-bold">
                  <span className="fa fa-exclamation text-danger"></span> Important message
                </div>
                <div className="small text-muted text-truncate">Lorem ipsum dolor sit amet, consecteur sit ament kollago ...</div>
              </div>
              </DropdownItem>
              <DropdownItem>
              <div className="message">
                <div className="py-3 mr-3 float-left">
                  <div className="avatar">
                    <img className="img-avatar" src="" alt="" />
                    <span className="avatar-status badge-success"></span>
                  </div>
                </div>
                <div>
                  <small className="text-muted">John Doe</small>
                  <small className="text-muted float-right mt-1">Just now</small>
                </div>
                <div className="text-truncate font-weight-bold">
                  <span className="fa fa-exclamation text-danger"></span> Important message
                </div>
                <div className="small text-muted text-truncate">Lorem ipsum dolor sit amet, consecteur sit ament kollago ...</div>
              </div>
              </DropdownItem>
              <DropdownItem>
              <div className="message">
                <div className="py-3 mr-3 float-left">
                  <div className="avatar">
                    <img className="img-avatar" src="" alt="" />
                    <span className="avatar-status badge-success"></span>
                  </div>
                </div>
                <div>
                  <small className="text-muted">John Doe</small>
                  <small className="text-muted float-right mt-1">Just now</small>
                </div>
                <div className="text-truncate font-weight-bold">
                  <span className="fa fa-exclamation text-danger"></span> Important message
                </div>
                <div className="small text-muted text-truncate">Lorem ipsum dolor sit amet, consecteur sit ament kollago ...</div>
              </div>
              </DropdownItem>
              <DropdownItem>
              <div className="message">
                <div className="py-3 mr-3 float-left">
                  <div className="avatar">
                    <img className="img-avatar" src="" alt="" />
                    <span className="avatar-status badge-success"></span>
                  </div>
                </div>
                <div>
                  <small className="text-muted">John Doe</small>
                  <small className="text-muted float-right mt-1">Just now</small>
                </div>
                <div className="text-truncate font-weight-bold">
                  <span className="fa fa-exclamation text-danger"></span> Important message
                </div>
                <div className="small text-muted text-truncate">Lorem ipsum dolor sit amet, consecteur sit ament kollago ...</div>
              </div>
              </DropdownItem>
            </DropdownMenu>
          </AppHeaderDropdown>
          <AppHeaderDropdown direction="down">
            <DropdownToggle nav>
              <img
                src={"../../assets/img/avatars/6.jpg"}
                className="img-avatar"
                alt="admin@bootstrapmaster.com"
              />
            </DropdownToggle>
            <DropdownMenu right style={{ right: "auto" }}>
              <DropdownItem
                header
                tag="div"
                className="text-center account-dropdown"
              >
                <strong>Account</strong>
              </DropdownItem>
              <DropdownItem>
                <i className="fa fa-user" /> Profile
              </DropdownItem>
              <DropdownItem onClick={this.handleLogoutClick}>
                <i className="fa fa-lock" /> Logout
              </DropdownItem>
            </DropdownMenu>
          </AppHeaderDropdown>
        </Nav>
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default inject("authStore")(observer(DefaultHeader));
